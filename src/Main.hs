module Main where

import Control.Monad (forever) -- [1]
import Data.Char (toLower) -- [2]
import Data.Maybe (isJust) -- [3]
import Data.List (intersperse) -- [4]
import System.Exit (exitSuccess) -- [5]
import System.Random (randomRIO) -- [6]
import Data.List (intersect)

type WordList = [String]

data Puzzle = Puzzle String [Maybe Char] [Char]
instance Show Puzzle where
  show (Puzzle _ discovered guessed) =
    (intersperse ' ' $ fmap renderPuzzleChar discovered)
    ++ "\nGuessed so far: " ++ guessed

allWords :: IO WordList
allWords = do dict <- readFile "../data/countries.txt"
              return $ lines dict

minWordLength :: Int
minWordLength = 2

maxWordLength :: Int
maxWordLength = 20

gameWords :: IO WordList
gameWords = do aw <- allWords
               return $ filter (\word -> length word >= minWordLength
                   && length word <= maxWordLength) aw

randomWord :: WordList -> IO String
randomWord wl = do randomIndex <- randomRIO (0, length wl - 1)
                   return $ wl !! randomIndex

randomWord' :: IO String
randomWord' = gameWords >>= randomWord

freshPuzzle :: String -> Puzzle
freshPuzzle w = Puzzle w (fmap (const Nothing) w) []

charInWord :: Puzzle -> Char -> Bool
charInWord (Puzzle w _ _) c = elem c w

alreadyGuessed :: Puzzle -> Char -> Bool
alreadyGuessed (Puzzle _ _ g) c = elem c g

renderPuzzleChar :: Maybe Char -> Char
renderPuzzleChar Nothing = '_'
renderPuzzleChar (Just c) = c

fillInChar :: Puzzle -> Char -> Puzzle
fillInChar (Puzzle w filledIn g) c =
  Puzzle w newFilledIn (c : g)
  where
    zipper guessed wordChar guessChar = if wordChar == guessed
                                        then Just wordChar
                                        else guessChar

    newFilledIn = zipWith (zipper c) w filledIn

handleGuess :: Puzzle -> Char -> IO Puzzle
handleGuess puzzle guess = do
  putStrLn $ "Your guess was: " ++ [guess]
  case (charInWord puzzle guess, alreadyGuessed puzzle guess) of
    (_, True) -> do
      putStrLn "You already guessed that\
                \ character, pick something else!"
      return puzzle
    (True, _) -> do
      putStrLn "You got it!,\
                \ filling in the word."
      return (fillInChar puzzle guess)
    (False, _) -> do
      putStrLn "Sorry, this character isn't in\
                \ the word, try again."
      return (fillInChar puzzle guess)

noOfGuesses :: Int
noOfGuesses = 5

gameOver :: Puzzle -> IO ()
gameOver (Puzzle wordToGuess _ guessed) 
  |guessedWrong >= noOfGuesses = do putStrLn "You lose!"
                                    putStrLn $ "The word was: " ++ intersperse ' ' wordToGuess
                                    exitSuccess
  |otherwise        = return ()
  where
    guessedWrong = length guessed - length [x | x <- guessed, elem x wordToGuess]

gameWin :: Puzzle -> IO ()
gameWin (Puzzle word filledIn _) 
  |all isJust filledIn = do putStrLn "You win!"
                            putStrLn $ "The word was " ++ intersperse ' ' word
                            exitSuccess
  |otherwise           = return ()

runGame :: Puzzle -> IO ()
runGame puzzle @ (Puzzle wordToGuess _ guessed) = forever $ do
  putStrLn "\n"
  gameWin puzzle
  gameOver puzzle
  putStrLn $ "Current puzzle is: " ++ show puzzle
  putStrLn $ "Number of wrong guesses left: "
  print guessesLeft
  putStrLn "Guess a letter: "
  guess <- getLine
  case guess of
    [c] -> handleGuess puzzle c >>= runGame
    _   -> putStrLn "Your guess must\
                     \ be a single character."
  where
    guessesLeft = noOfGuesses - (length guessed - length [x | x <- guessed, elem x wordToGuess])

main :: IO ()
main = do word <- randomWord'
          let puzzle = freshPuzzle (fmap toLower word)
          runGame puzzle
